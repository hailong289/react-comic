import React from 'react';
import './App.scss';
import SwitchRouter from './core/router/switchrouter';
import { Navbar } from './public/component/navbar/navbar';
function App() {
    return (
        <div className="App">
            <Navbar />
            <SwitchRouter />
        </div>
    );
}

export default App;
