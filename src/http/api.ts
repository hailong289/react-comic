import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';

const api = axios.create({
	baseURL: process.env.REACT_APP_BASE_URL_API,
	headers: {
		'Content-Type': 'application/json',
		Accept: 'application/json',
	},
});

api.interceptors.request.use(
	function (config: AxiosRequestConfig | any) {
		config.headers.Authorization = `Bearer ${
			JSON.parse(localStorage.getItem('hlona') || 'null')?.access_token
		}`;
		return config;
	},
	function (error: AxiosError) {
		return error;
	}
);

api.interceptors.response.use(
	function (response: AxiosResponse) {
		return response.data;
	},
	function (error: AxiosError) {
		return Promise.reject(error);
	}
);

export default api;
