import { useToast } from "@chakra-ui/react";
import { useState, useEffect } from "react";

type ToastState = {
	message: string | null;
	description?: string;
	status: "info" | "warning" | "success" | "error";
	position?: "top" | "bottom" | "top-left" | "top-right" | "bottom-left" | "bottom-right";
};

export function useToastHook() {
	const [state, setState] = useState<ToastState | any>(null);
    const toast = useToast();
	useEffect(() => {
		if (state) {
			const { message, description, status, position } = state;
			toast({
				title: message,
				description: description,
				status: status,
				duration: 2500,
				position: position || "top",
				isClosable: true,
			});
		}
	}, [state]);

	return [state, setState];
}
