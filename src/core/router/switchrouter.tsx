import React from "react";
import { Route } from "react-router-dom";
import { Routes } from "react-router-dom";
import { router } from "./router";

export default function SwitchRouter() {
  return (
    <>
      <Routes>
         {router.map((router, i) => (
            <Route
               key={i}
               path={router.path}
               index={router.index ?? false}
               element={
               <React.Suspense fallback={<>...</>}>
                 {router.component}
               </React.Suspense>
               }
             >
               {router.children && router.children.map((chidren, i) => (
                  <Route
                  key={i}
                  path={chidren.path}
                  index={chidren.index ?? false}
                  element={
                  <React.Suspense fallback={<>...</>}>
                    {chidren.component}
                  </React.Suspense>
                  }
                >
                  {chidren.children && chidren.children.map((chidren, i) => (
                  <Route
                     key={i}
                     path={chidren.path}
                     index={chidren.index ?? false}
                     element={
                     <React.Suspense fallback={<>...</>}>
                     {chidren.component}
                     </React.Suspense>
                     }
                  />))}
                </Route>
               ))}
             </Route>
          ))}
      </Routes>
    </>
  );
}
