import React from "react";
import { RouterConfig } from "@model/router";

const Layout = React.lazy(() => import('@app/layout'));
const Home = React.lazy(() => import('@app/component/home'));
const List = React.lazy(() => import('@app/component/list/List'));
const Courses = React.lazy(() => import('@app/component/cousers/couser').then(({ Courses }) => ({ default: Courses })));
const CoursesIndex = React.lazy(() => import('@app/component/cousers/couser').then(({ CoursesIndex }) => ({ default: CoursesIndex })));
const Course = React.lazy(() => import('@app/component/cousers/couser').then(({ Course }) => ({ default: Course })));
// const NoMatch = React.lazy(() => import('../../public/component/cousers/couser').then(({ NoMatch }) => ({ default: NoMatch })));
const NotFound = React.lazy(() => import('@app/page/error/notFound'));
const Comic = React.lazy(()=>import('@app/component/comic/comic'));

export const router: RouterConfig[] = [
    {
        path: '/',
        component: <Layout />,
        index: false,
        children: [
            { path: '', component: <Home />, index: true },
            { path: 'danh-sach', component: <List /> },
            { path: ':id', component: <Comic />}
        ]
    },
    {
      path: "/danh-muc",
      component: <Courses />,
      children: [
        { path: '', index: true, component: <CoursesIndex /> },
        { path: "/danh-muc/:id", component: <Course /> },
      ],
    },

    { path: "*", component: <NotFound />},
]
