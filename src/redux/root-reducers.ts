import { combineReducers } from "redux";
import loginReducer from "@features/auth/loginReducer";

const rootReducer = combineReducers({
    login: loginReducer
});

export default rootReducer;
