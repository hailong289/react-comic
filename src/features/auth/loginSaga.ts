import { loginAction } from './loginReducer';
import { put, takeLatest } from "redux-saga/effects";


function generate_token(data: any = null, length = 500){
    const string = data.username + data.password
    //edit the token allowed characters
    var a = (string + "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890").split("");
    var b = [];
    for (var i=0; i<length; i++) {
        var j: any = (Math.random() * (a.length-1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
}

function* login(actions: any){
    const {username, password } = actions.payload;
    if(username === 'long' && password === '12345'){
        const users = {
           id: 1,
           name: 'long',
           data_login: new Date().toISOString().slice(0, 10),
        };
        localStorage.setItem('hlona', JSON.stringify({token: generate_token(actions.payload)}));
        yield put(loginAction.success(users));
    }else{
        yield put(loginAction.error());
    }

}


export default function* loginSaga(){
    yield takeLatest(loginAction.login, login);
}
