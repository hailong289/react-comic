export interface AuthState {
    users: User |  null,
    status: 'loading' | 'error' | 'success',
    message: string | null
}


export interface User {
    id?: number,
    name?: string,
    username: string;
    password?: string;
    date_login?: Date;
}
