import { RootState } from '@redux/store';
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AuthState, User } from './loginState';

const states: AuthState = {
    users: null,
    status: 'loading',
    message: null
}


const loginSlice = createSlice({
    name: 'login',
    initialState: states,
    reducers:{
        login: (state, aciton: PayloadAction<User>)=>{
            state.status = 'loading';
        },
        success: (state, actions: PayloadAction<any>)=>{
            state.status = 'success';
            state.users = actions.payload;
        },
        error: (state) => {
            state.status = 'error';
        }
    }
});


// Action
export const loginAction = loginSlice.actions;


// Selectors

export const SelectUsers = (state: RootState) => state.login.users;
export const Selectstatus = (state:  RootState) => state.login.status;


//Reducer
const loginReducer = loginSlice.reducer;
export default loginReducer;
