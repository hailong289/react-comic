import {RxDatabase, createRxDatabase} from 'rxdb';
import { getRxStorageDexie } from 'rxdb/plugins/storage-dexie';
import { mySchema } from './local/table';


export const Database: any = {
    create: async function() {
        this.db = await createRxDatabase({
            name: 'todo_db',
            storage: getRxStorageDexie()
        });

        await this.db.addCollections({
            todos: {
              schema: mySchema
            },
            table2: {
                schema: mySchema
            }
        });
    }
}
