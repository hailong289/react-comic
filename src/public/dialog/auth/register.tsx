import {
    Button,
    FormControl,
    FormLabel,
    Input,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay
} from "@chakra-ui/react"
import React from "react"



export default function Register(props: any) {
    const { isOpenMode,closeModal } = props;



    const initialRef = React.useRef(null);
    const finalRef = React.useRef(null);

    const onClose = () => {closeModal()}

    return (
      <>
        <Modal
          isCentered
          size="sm"
          initialFocusRef={initialRef}
          finalFocusRef={finalRef}
          isOpen={isOpenMode}
          onClose={onClose}
        >
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Tạo tài khoản</ModalHeader>
            <ModalCloseButton />
            <ModalBody pb={6}>
              <FormControl>
                <FormLabel>Tên đăng nhập</FormLabel>
                <Input ref={initialRef} placeholder='Tên đăng nhập' />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Email</FormLabel>
                <Input placeholder='Email' />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Mật khẩu</FormLabel>
                <Input placeholder='Mật khẩu' />
              </FormControl>
              <FormControl mt={4}>
                <FormLabel>Nhập lại mật khẩu</FormLabel>
                <Input placeholder='Nhập lại mật khẩu' />
              </FormControl>
            </ModalBody>

            <ModalFooter>
              <Button colorScheme='blue' mr={3}>
                Đăng ký
              </Button>
              <Button onClick={onClose}>Thoát</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    )
}
