import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay
} from "@chakra-ui/react"
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Selectstatus, loginAction, SelectUsers } from "@features/auth/loginReducer";
import { useAppSelector } from "@redux/hook";
import { useToastHook } from '@core/message';



export default function Login(props: any) {
  const { isOpenMode, closeModal } = props;
  const [form, setForm] = useState({
    username: '',
    password: ''
  });
  const users = useAppSelector(SelectUsers);
  const Loginstatus = useAppSelector(Selectstatus);
  const dispatch = useDispatch();

  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);
  const [status, setStatus] = React.useState(Loginstatus);
  const onClose = () => { closeModal() };
  const [state, newToast] = useToastHook();


  const handleUser = (e: any) => setForm({ ...form, username: e.target.value });
  const handlePassword = (e: any) => setForm({ ...form, password: e.target.value });

  React.useEffect(() => {
    setStatus(Loginstatus);
  }, [Loginstatus]);

  const onSubmit = async () => {
    dispatch(loginAction.login(form));
    if (status === 'success') {
      closeModal();
      newToast({ message: 'Đăng nhập thành công', status: status });
    } else if (status === 'error') {
      newToast({ message: 'Có lỗi xảy ra', status: status });
    }
  }

  return (
    <>
      <Modal
        isCentered
        size="sm"
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpenMode}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Đăng nhập</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Tên đăng nhập</FormLabel>
              <Input ref={initialRef} value={form.username} onChange={handleUser} placeholder='Tên đăng nhập' />
            </FormControl>
            <FormControl mt={4}>
              <FormLabel>Mật khẩu</FormLabel>
              <Input placeholder='Mật khẩu' value={form.password} onChange={handlePassword} />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme='blue' mr={3} onClick={onSubmit}>
              Đăng nhập
            </Button>
            <Button onClick={onClose}>Thoát</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}
