
import { BookOpenIcon, MenuIcon } from "@heroicons/react/outline";
import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import Login from "../../dialog/auth/login";
import Register from "../../dialog/auth/register";
import "./navbar.scss";
import { Database } from "@model/database";
export function Navbar() {
    const [hState,sethState] = useState("top");
    const [classHeader,setClassHeader] = useState("relative");
    const handleMenu = (e: any) => {
        const checked = e.target.checked ?? false;
        const menu_mobile = document.getElementsByClassName('menu_mobile')[0];
        if (checked) {
            menu_mobile.classList.add("show");
        } else {
            menu_mobile.classList.remove("show");
        }
    }

    window.addEventListener('beforeunload', ()=>{
        const menu_mobile = document.getElementsByClassName('menu_mobile');
        if(menu_mobile && menu_mobile[0]) menu_mobile[0].classList.remove("show");
    });

    const [modalLogin, setModalLogin] = React.useState(false);
    const [modalRegister, setModalRegister] = React.useState(false);

    useEffect(()=>{
        var lastVal = 0
        window.onscroll = function(){
            let y = window.scrollY
            // if(y > lastVal){sethState("down")}
            // if(y < lastVal) {sethState("up")}
            // if(y === 0) {sethState("top")}
            lastVal = y;
            if(lastVal > 100){
                setClassHeader('fixed top-0 w-full z-[100]');
            }else{
                setClassHeader('relative');
            }
        }
    },[]);



    const handleLogin = async () => {
        // const myDocument = await Database.db.todos.insert({
        //     name: 'Learn RxDB',
        //     done: false,
        //     timestamp: new Date().toISOString()
        // });
        setModalLogin(true);
    }

    const handleRegister = () => {
        setModalRegister(true);
    }

    const onClose = () => {
        setModalLogin(false);
        setModalRegister(false);
    }

    return (
        <div className={`flex basic justify-around items-center bg-amber-300 h-14 ${classHeader}`}>
            <div className="basis-[10%] menu_pc">
                <div className="rounded-full logo_company">
                    <BookOpenIcon className="h-5 w-5 text-blue-500" />
                </div>
            </div>
            <div className="basis-[80%] menu_pc">
                <ul className="menu">
                    <li><Link to="/">Trang chủ</Link></li>
                    <li><Link to="/danh-muc">Danh mục</Link></li>
                    <li><Link to="/hot">Hot</Link></li>
                    <li><Link to="/dang-theo-doi">Đang theo dõi</Link></li>
                    <li><Link to="/dich-truyen">Dịch truyện</Link></li>
                </ul>
            </div>
            <div className="basis-[10%] menu_pc">
                <div className="dropdown inline-block relative float-right mr-4">
                    <div className="bg-sky-600 text-white font-semibold py-2 px-4 rounded inline-flex items-center">
                        <span className="mr-1">Đăng nhập</span>
                        <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /> </svg>
                    </div>
                    <ul className="dropdown-menu absolute hidden text-gray-700 pt-1">
                        <li className="item"><p className="rounded-t bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" >One</p></li>
                        <li className="item"><p className="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" >Two</p></li>
                        <li className="item"><p className="rounded-b bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap">Three is the magic number</p></li>
                    </ul>
                </div>
            </div>
            <div className={"menu_mobile flex flex-col down " + hState}>
                <div className="item mt-3"><Link to="/">Trang chủ</Link></div>
                <div className="item"><Link to="/danh-muc">Danh mục</Link></div>
                <div className="item"><Link to="/hot">Hot</Link></div>
                <div className="item"><Link to="/dang-theo-doi">Đang theo dõi</Link></div>
                <div className="item"><Link to="/dich-truyen">Dịch truyện</Link></div>
                <div className="item" onClick={handleRegister}>Đăng ký</div>
                <div className="item" onClick={handleLogin}>Đăng nhập</div>
            </div>
            <label htmlFor="menu_child" className="icon_menu_mb fixed right-[20px]"><MenuIcon className="h-8 w-8 text-white" /></label>
            <input type="checkbox" id="menu_child" onChange={handleMenu} />
            <Login isOpenMode={modalLogin} closeModal={onClose} />
            <Register isOpenMode={modalRegister} closeModal={onClose} />
        </div>
    );
}
