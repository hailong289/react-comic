import {
    AbsoluteCenter,
    Box,
    Button,
    Card,
    CardBody,
    CardHeader,
    Divider,
    Flex,
    Stack,
    StackDivider,
    Text,
    Textarea
} from "@chakra-ui/react";

export default function Comic() {
    return (
        <>
            <section className="lg:flex lg:justify-center row-comic flex-wrap lg:shadow-md lg:rounded-xl">
                <div className="lg:mr-2 p-4 lg:flex lg:max-w-6xl lg:w-full">
                    <div className="lg:w-1/4 py-2">
                        <div className="h-[450px] bg-cover lg:h-full bg-[url('https://images.unsplash.com/photo-1517245386807-bb43f82c33c4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80')]"></div>
                    </div>

                    <div className="max-w-xl lg:px-5 lg:max-w-5xl lg:w-3/4">
                        <h2 className="text-2xl font-semibold text-gray-800 dark:text-white md:text-3xl">
                            Thăng cấp một mình
                        </h2>

                        {/* <p className="mt-4 text-gray-500 dark:text-gray-300">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                            Quidem modi reprehenderit vitae exercitationem aliquid dolores ullam temporibus enim expedita aperiam
                            mollitia iure consectetur dicta tenetur, porro consequuntur saepe accusantium consequatur.
                        </p> */}
                        <ul className="lg:mt-4 p-0 flex flex-col gap-[6px]">
                            <li className="list-none">
                                <span className="mr-2 text-gray-700">
                                    Ngày đăng:
                                </span>
                                <span className="text-gray-500">
                                    2022-01-06
                                </span>
                            </li>
                            <li className="list-none">
                                <span className="mr-2 text-gray-700">
                                    Trạng thái:
                                </span>
                                <span className="">Đang cập nhật</span>
                            </li>
                            <li className="list-none">
                                <span className="mr-2 text-gray-700">
                                    Tác giả:
                                </span>
                                <span className="text-gray-500">Long</span>
                            </li>
                            <li className="list-none">
                                <span className="mr-2 text-gray-700">
                                    Dịch giả:
                                </span>
                                <span className="text-gray-500">Long</span>
                            </li>
                            <li className="list-none">
                                <span className="mr-2 text-gray-700">
                                    Thể loại:
                                </span>
                                <span className="text-gray-500">
                                    Manhwa, Actions
                                </span>
                            </li>
                            <li className="list-none">
                                <span className="mr-2 text-gray-700">
                                    Lượt xem:
                                </span>
                                <span className="text-gray-500">
                                    Manhwa, Actions
                                </span>
                            </li>
                            <li className="list-none">
                                <span className="mr-2 text-gray-700">
                                    Đánh giá:
                                </span>
                                <span className="text-gray-500">******</span>
                            </li>
                        </ul>

                        <div className="inline-flex w-full mt-6 sm:w-auto">
                            <a
                                href="#url"
                                className="inline-flex items-center justify-center w-auto px-4 py-2 mr-2 text-sm text-white duration-300 bg-green-400 rounded-lg hover:bg-green-400 focus:ring focus:ring-gray-300 focus:ring-opacity-80"
                            >
                                Đọc truyện
                            </a>
                            <a
                                href="#url"
                                className="inline-flex items-center justify-center w-auto px-4 py-2 mr-2 text-sm text-white duration-300 bg-yellow-400 rounded-lg hover:bg-yellow-400 focus:ring focus:ring-gray-300 focus:ring-opacity-80"
                            >
                                Theo dõi
                            </a>
                        </div>
                    </div>
                </div>
                <div className="w-full">
                    <Box position="relative" padding="2" mt={"4"}>
                        <Divider />
                        <AbsoluteCenter bg="white" px="4" fontSize={"18px"}>
                            Mô tả
                        </AbsoluteCenter>
                    </Box>
                    <Box padding="3">
                        <Text>Nội dung truyện</Text>
                    </Box>
                    <Box position="relative" padding="2" mt={"4"}>
                        <Divider />
                        <AbsoluteCenter bg="white" px="4" fontSize={"18px"}>
                            Chương
                        </AbsoluteCenter>
                    </Box>
                    <div className="w-100 mt-4">
                        <Card m={"2"}>
                            <CardHeader pb={"0"}>
                                <Flex justifyContent={"space-between"}>
                                    <div className={"text-[16px]"}>Chapter</div>
                                    <div className={"text-[16px]"}>
                                        Ngày cập nhật
                                    </div>
                                </Flex>
                            </CardHeader>
                            <CardBody>
                                <Stack divider={<StackDivider />} spacing="2">
                                    <Box>
                                        <Flex justifyContent={"space-between"}>
                                            <Text pt="2" fontSize="sm">
                                                Chapter 1
                                            </Text>
                                            <Text
                                                pt="2"
                                                fontSize="sm"
                                                className={"text-gray-500"}
                                            >
                                                28/01/2023
                                            </Text>
                                        </Flex>
                                    </Box>
                                    <Box>
                                        <Flex justifyContent={"space-between"}>
                                            <Text pt="2" fontSize="sm">
                                                Chapter 2
                                            </Text>
                                            <Text
                                                pt="2"
                                                fontSize="sm"
                                                className={"text-gray-500"}
                                            >
                                                28/02/2023
                                            </Text>
                                        </Flex>
                                    </Box>
                                </Stack>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="w-full mt-4 p-2">
                        <Box position="relative" padding="2" mb={"4"}>
                            <Divider />
                            <AbsoluteCenter bg="white" px="4" fontSize={"18px"}>
                                Bình luận
                            </AbsoluteCenter>
                        </Box>
                        <Textarea placeholder="Viết gì đó" />
                        <Button colorScheme="blue" className="mt-2">
                            Gửi
                        </Button>
                    </div>
                </div>
            </section>
        </>
    );
}
