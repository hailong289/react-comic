
import "./page";
export default function Page(props: any) {
    const limit = props.limit ?? 10;
    const page = props.page ?? 1;
    const page_current = props.page_current ?? 1;
    const total = props.total ?? 0;
    const total_page = Math.ceil(total / limit);

    let arr_page = [];
    for (let i = 0; i < total_page; i++) {
        arr_page.push(i);
    }
    console.log(arr_page);

    // const renderPage = (total_page: number, page: number) => {
    //     let content = [];
    //     for (let i = 0; i < total_page; i++) {
    //         // bg-blue-500 text-white
    //         const active = i == page ? 'bg-blue-500 text-white':'';
    //         content.push();
    //     }
    //     return content;
    // }

    const handleChangePage = (page:number = 1, type: string | null = null) => {
        if(!type){}
    }

    return (
        <div className="flex p-20 justify-center" >
            {(page <= 1) ? (<a href="#"  onClick={() => handleChangePage()}
                className="px-4 py-2 mx-1 text-gray-500 capitalize bg-white rounded-md cursor-not-allowed dark:bg-gray-800 dark:text-gray-600 shadow-2xl">
                <div className="flex items-center -mx-1">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6 mx-1 rtl:-scale-x-100" fill="none"
                        viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M7 16l-4-4m0 0l4-4m-4 4h18" />
                    </svg>
                </div>
            </a>):''}
            {arr_page.map((row, i) => (
                <a href="#" key={i} className="hidden px-4 py-2 mx-1 text-gray-700 transition-colors duration-300 transform bg-white rounded-md sm:inline dark:bg-gray-800 dark:text-gray-200 shadow-2xl"
                onClick={() => handleChangePage(row)}>
                 {row}
               </a>
            ))}
            {(page < total_page) ? (<a href="#"
                className="px-4 py-2 mx-1 text-gray-700 transition-colors duration-300 transform bg-white rounded-md dark:bg-gray-800 dark:text-gray-200 hover:bg-blue-500 dark:hover:bg-blue-500 hover:text-white dark:hover:text-gray-200 shadow-2xl">
                <div className="flex items-center -mx-1">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6 mx-1 rtl:-scale-x-100" fill="none"
                        viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17 8l4 4m0 0l-4 4m4-4H3" />
                    </svg>
                </div>
            </a>):''}
        </div>
    );
}
