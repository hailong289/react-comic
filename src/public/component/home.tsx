import './home.scss';

export default function Home() {
  const data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12];
  return (
    <div className="list-comic-new flex flex-wrap">
      {data.map((e, i) => (
        <div className="card-item px-2 mb-4" key={i}>
          <div className="image_comic w-full overflow-hidden rounded-[12px] shadow-lg">
            <img src="https://i.pinimg.com/736x/4d/d3/e5/4dd3e578d7bf8657b305acfd344e67f7.jpg" alt="" className="w-full h-full object-cover" />
          </div>
          <div className="content w-full mt-1 px-2">
            <p className='name_comic text-left text-[18px] cursor-pointer'>Tôi thăng cấp một mình</p>
            <div className='list-chapter mt-2'>
              <div className='chapter-time flex'>
                <span className='mr-auto text-black'>Chapter 1</span>
                <small>10 giờ trước</small>
              </div>
              <div className='chapter-time flex'>
                <span className='mr-auto text-black'>Chapter 1</span>
                <small>10 giờ trước</small>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}
