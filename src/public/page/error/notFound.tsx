import "./notfound.scss";

export default function NotFound() {
    return (
        <div className="bg-[url('https://bit.ly/3kY3hQa')] h-screen bg-cover bg-bottom relative">
            <div
                className="max-w-screen-xl m-auto flex flex-col px-4 sm:items-center sm:pb-8 pb-4 h-screen pt-14 sm:pt-44 relative z-5">
                <h2 className="max-w-screen-md text-center sm:text-8xl font-extrabold text-white text-6xl"> 404 </h2>
                <h4 className="max-w-screen-md text-center text-white sm:my-14 my-8 text-xl sm:text-3xl"> Không tìm thấy địa chỉ</h4>
                <div>
                    <div className="flex gap-2.5 justify-center sm:mx-4">
                        <button className="py-3 px-4 text-white duration-100 rounded-xl text-lg font-semibold bg-slate-300 hover:bg-violet-400"> Trở lại </button>
                    </div>
                </div>
            </div>
            <div className="absolute w-full h-full top-0 bottom-0 z-2 opacity-70 bg-black pointer-events-none"></div>
        </div>
    );
}
